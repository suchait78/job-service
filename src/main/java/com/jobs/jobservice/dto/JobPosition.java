package com.jobs.jobservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class JobPosition {

    @JsonProperty("job_id")
    private Long jobId;

    @JsonProperty("job_name")
    private String jobName;

    @JsonProperty("job_description")
    private String jobDescription;

    @JsonProperty("job_location")
    private String jobLocation;

    @JsonProperty("positions")
    private Long positions;

    @JsonProperty("organization_name")
    private String organizationName;

}
