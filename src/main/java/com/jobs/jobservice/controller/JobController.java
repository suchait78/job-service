package com.jobs.jobservice.controller;


import com.jobs.jobservice.dto.JobPosition;
import com.jobs.jobservice.exception.JobPositionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@RestController
public class JobController {

    private static final Set<JobPosition> jobPositionList;

    static {
        jobPositionList = new HashSet<>();
        jobPositionList.add(new JobPosition(12L, "Analyst", "Description-1", "Pune", 10L, "Company-1"));
        jobPositionList.add(new JobPosition(13L, "Developer", "Description-2", "Gurgaon", 10L, "Company-12"));
        jobPositionList.add(new JobPosition(14L, "Business Analyst", "Description-3", "Bangalore", 10L, "Company-19"));
        jobPositionList.add(new JobPosition(15L, "Manager", "Description-4", "Pune", 10L, "Company-17"));
        jobPositionList.add(new JobPosition(16L, "Technical Support", "Description-5", "Pune", 10L, "Company-100"));
    }

    @GetMapping("/all/positions")
    public Set<JobPosition> getAllJobs() {
        return jobPositionList;
    }

    @GetMapping("/position/{id}")
    public JobPosition getPositionWithId(@PathVariable Long id) throws JobPositionException {

        Optional<JobPosition> jobPosition = Optional.ofNullable(jobPositionList.stream()
                .filter(jobRecord -> jobRecord.getJobId().equals(id))
                .findFirst()
                .orElseThrow(() -> new JobPositionException("No data found.")));

        return jobPosition.get();
    }

    @PostMapping("/create")
    public List<JobPosition> createJobPositions(@RequestBody List<JobPosition> requestJobPositionList) throws JobPositionException {

        if (requestJobPositionList.isEmpty()) {
            log.error("Throwing exception");
            throw new JobPositionException("JobPosition data can't be empty.");
        }

        log.info("Adding jobs {} ", requestJobPositionList);
        jobPositionList.addAll(requestJobPositionList);
        return requestJobPositionList;
    }

    @PutMapping("/update")
    public List<JobPosition> updateJobPositions(@RequestBody List<JobPosition> requestJobPositionList) throws JobPositionException {

        if (requestJobPositionList.isEmpty()) {
            throw new JobPositionException("JobPosition data can't be empty.");
        }

        List<Long> idsList =
                requestJobPositionList.stream()
                        .map(pos -> pos.getJobId())
                        .collect(Collectors.toList());

        List<JobPosition> jobPositionListResult =
                jobPositionList.stream()
                        .filter(pos -> idsList.contains(pos.getJobId()))
                        .collect(Collectors.toList());

       for(JobPosition pos : jobPositionListResult) {
                    jobPositionList.remove(pos);
       }

        log.info("Updating the objects now.");
        jobPositionList.addAll(requestJobPositionList);

        return requestJobPositionList;
    }

}
