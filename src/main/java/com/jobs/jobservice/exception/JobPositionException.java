package com.jobs.jobservice.exception;

public class JobPositionException extends Exception{

    public JobPositionException(String message) {
        super(message);
    }
}
