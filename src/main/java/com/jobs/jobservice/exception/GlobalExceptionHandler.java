package com.jobs.jobservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(JobPositionException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApplicationException handleJobPositionException(JobPositionException ex, WebRequest request) {

        ApplicationException applicationException =
                new ApplicationException("ERR-101",ex.getMessage(),"Something is wrong with JobPosition"
                , LocalDateTime.now());

        return applicationException;
    }

}
