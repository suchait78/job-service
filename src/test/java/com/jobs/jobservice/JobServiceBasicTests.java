package com.jobs.jobservice;

import com.jobs.jobservice.controller.JobController;
import com.jobs.jobservice.dto.JobPosition;
import com.jobs.jobservice.exception.JobPositionException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JobServiceBasicTests {

    @Autowired
    private JobController jobController;

    @Test
    public void testJobPositionService() throws JobPositionException {

        List<JobPosition> positionList = new ArrayList<>();

        positionList.add(new JobPosition(101L, "CTO", "Description-1", "Bangalore", 2L, "Company-1"));
        positionList.add(new JobPosition(102L, "CEO", "Description-2", "Bangalore", 2L, "Company-12"));

        List<JobPosition> posList = jobController.createJobPositions(positionList);
        Assert.assertNotNull(posList);
    }
}
